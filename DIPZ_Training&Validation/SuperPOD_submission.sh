#!/bin/bash

#SBATCH -J retraining_DIPZ_superpod       # job name to display in squeue
#SBATCH -o output-retraining_DIPZ.txt    # standard output file
#SBATCH -e error-retraining_DIPZ.txt     # standard error file
#SBATCH -p batch     # requested partition
#SBATCH --mem=100G          # Total memory required
#SBATCH -D /users/maboelela/DIPZ/the_DIPZ_project/DIPZ_Training&Validation  #sets the working directory where the batch script should be run
#SBATCH -s   #tells SLURM that the job can not share nodes with other running jobs
#SBATCH --gres=gpu:4                 # number of GPUs gpu:1 = 1 gpu, gpu2:2 = 2 gpu
#SBATCH --mail-user maboelela@smu.edu   #tells SLURM your email address if you’d like to receive job-related email notifications
#SBATCH --mail-type=all

# load conda and activate environment
module purge
module load conda
conda activate dipz

# load cuda libraries
module load gcc/11.2.0
module load cuda
module load cudnn

# run the notebook
# This command is usually used to convert a notebook to a python script,
# but we can also use it to run the notebook and write the output into 
# the same notebook, so when you open it the output areas are populated
#jupyter nbconvert --to notebook --inplace --execute DIPZ_Training.ipynb
time python DIPZ_Training.py -t -e
