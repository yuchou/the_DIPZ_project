#!/bin/bash

#SBATCH -J mlpl_2_all       # job name to display in squeue
#SBATCH -o output-mlpl_2_all.txt    # standard output file
#SBATCH -e error-mlpl_2_all.txt     # standard error file
#SBATCH -p standard-s      # requested partition
#SBATCH --mem=500G          # Total memory required
#SBATCH -t 1400              # maximum runtime in minutes
#SBATCH -D /users/maboelela/Research/Qualification_Task/The_DIPZ_Project/Fast_Rejection_Scheme/MLPL_Distributions/mlpl_2_all  #sets the working directory where the batch script should be run
#SBATCH -s   #tells SLURM that the job can not share nodes with other running jobs
#SBATCH --mail-user maboelela@smu.edu   #tells SLURM your email address if you’d like to receive job-related email notifications
#SBATCH --mail-type=all

module purge
module load conda
eval "$(conda shell.bash hook)"
conda activate /lustre/work/client/users/maboelela/.conda/envs/dipz

time python run_and_save.py -t -e